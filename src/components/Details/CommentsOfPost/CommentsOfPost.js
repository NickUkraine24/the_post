import React, { Component } from 'react';
import {
  Media, 
  Container, 
  Row, 
  Col, 
  Card } from 'reactstrap';

import './CommentsOfPost.scss';

class CommentsOfPost extends Component {
  render() {
    return (
      <Container>
        <Row>
          <Col sm="12" md={{ size: 10, offset: 1 }}>
            <Card>
            <Media className="media_comment">
              <Media heading className="heading_media">
                <div className="div_name">
                  {this.props.postComments.name}
                </div>
                <div className="div_email">
                  {this.props.postComments.email}
                </div>
              </Media>
              <div className="div_body">
                {this.props.postComments.body}
              </div>
            </Media>
            </Card>
            </Col>
        </Row>
      </Container>
      )
    }
}

export default CommentsOfPost;
