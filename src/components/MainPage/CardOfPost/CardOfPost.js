import React, { Component } from 'react';
import { 
  Card, 
  CardBody,
  CardTitle,
  Button } from 'reactstrap';

import { Link } from "react-router-dom";

import './CardOfPost.scss';

class CardOfPost extends Component {
  render() {
    const { postList: { id } } = this.props;
    // console.log('this.props.postList.title', this.props.postList.title)
    return (
      <Card className="div_card">
        <CardBody>
          <CardTitle className="title_card">{this.props.postList.title}</CardTitle>
          <Link to={`/details/${id}`}>
            <Button className="btn_card">More..</Button>
          </Link>
        </CardBody>
      </Card>
    );
  }
}

export default CardOfPost;
