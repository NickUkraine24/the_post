import axios from 'axios';
import * as ActionTypes from '../constans/ActionTypes';

export const getCommentFromAPI = () => {
  return dispatch => {
    axios.get(`https://jsonplaceholder.typicode.com/comments`)
      .then((res) => {
        dispatch({type: ActionTypes.GET_COMMENT_SUCCESS, payload: res.data})
      })
      .catch((err) => {
        dispatch({type: ActionTypes.GET_COMMENT_ERROR, payload: err.message})
      });
  }
};
