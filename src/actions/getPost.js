import axios from 'axios';
import * as ActionTypes from '../constans/ActionTypes';
import api from '../constans/api';

export const getPostFromAPI = () => {
  return dispatch => {
    axios.get(`https://jsonplaceholder.typicode.com/posts`)
      .then((res) => {
        dispatch({type: ActionTypes.GET_POST_SUCCESS, payload: res.data})
      })
      .catch((err) => {
        dispatch({type: ActionTypes.GET_POST_ERROR, payload: err.message})
      });
  }
};
