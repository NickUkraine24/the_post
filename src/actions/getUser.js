import axios from 'axios';
import * as ActionTypes from '../constans/ActionTypes';

export const getUserFromAPI = () => {
  return dispatch => {
    axios.get(`https://jsonplaceholder.typicode.com/users`)
      .then((res) => {
        dispatch({type: ActionTypes.GET_USER_SUCCESS, payload: res.data})
      })
      .catch((err) => {
        dispatch({type: ActionTypes.GET_USER_ERROR, payload: err.message})
      });
  }
};
