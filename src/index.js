import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './components/App/App';
import registerServiceWorker from './registerServiceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';

import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import reducer from './reducers/index';
import { composeWithDevTools } from 'redux-devtools-extension';

const store = createStore(reducer, composeWithDevTools( applyMiddleware(thunk) ));
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
  ,
document.getElementById('root'));

registerServiceWorker();
