import * as ActionTypes from '../constans/ActionTypes';

const initialState = {
    postList: []
};

export default function post(state = initialState, action) {
    switch (action.type) {
        case ActionTypes.GET_POST_SUCCESS:
          return {
            ...state,
            postList: action.payload
          }
        default:
          return state;
    }
}
