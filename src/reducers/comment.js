import { GET_COMMENT_SUCCESS, GET_COMMENT_ERROR } from '../constans/ActionTypes';

const initialState = {
    commentList: []
};

export default function comment(state = initialState, action) {
    switch (action.type) {
        case GET_COMMENT_SUCCESS:
          return {
            ...state,
            commentList: action.payload
          }
        case GET_COMMENT_ERROR:
          return {
            ...state,
            message: "Problem with comment data"
          }
        default:
          return state;
    }
}
