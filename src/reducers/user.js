import { GET_USER_SUCCESS, GET_USER_ERROR } from '../constans/ActionTypes';

const initialState = {
    userList: []
};

export default function user(state = initialState, action) {
  switch (action.type) {
    case GET_USER_SUCCESS:
      return {
        ...state,
        userList: action.payload
      }
    case GET_USER_ERROR:
      return {
        ...state,
        message: "Problem with user data"
      }
    default:
      return state;
  }
}
