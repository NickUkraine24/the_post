import { combineReducers } from 'redux';

import post from './post';
import user from './user';
import comment from './comment';

export default combineReducers({
  post,
  user,
  comment
})
