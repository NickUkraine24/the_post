import React, { Component } from 'react';

import { BrowserRouter as Router, Route, Redirect, Switch } from "react-router-dom";

import MainPage from '../MainPage/MainPage';
import About from '../About/About';
import Contact from '../Contact/Contact';
import Details from '../Details/Details';
import './App.scss';

class App extends Component{
  render(){
    return(
      <Router>
        <div>
          <Switch>
            <Route exact path='/home' component={MainPage} />
            <Route exact path='/about' component={About} />
            <Route exact path='/contact' component={Contact} />
            <Route path='/details/:id' component={Details} />  
            <Redirect to="/home" />
          </Switch>
        </div>
      </Router>
    )
  }
}

export default App;
