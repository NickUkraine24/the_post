import React, { Component } from 'react';
import {Link} from "react-router-dom";
import { 
  Container, 
  Row, 
  Col } from 'reactstrap';

import './Footer.scss';

class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <Container>
          <Row>
            <Col sm="12" md={{ size: 8, offset: 2 }}>
              <Link to="/about" className="footer_link_about">
                ∙ About
              </Link>
            </Col>
          </Row>
          <Row>
            <Col sm="12" md={{ size: 8, offset: 2 }} className="footer_about">
              <Link to="/contact" className="footer_link_contact">
                ∙ Contact
              </Link>
            </Col>
          </Row>
        </Container>
      </div>
      )
    }
}
export default Footer;
