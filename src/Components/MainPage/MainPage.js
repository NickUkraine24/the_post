import React, {Component} from 'react';
import { Container, Row, Col } from 'reactstrap';

import { getPostFromAPI } from '../../actions/getPost';
import { getUserFromAPI } from '../../actions/getUser';
import { connect }  from 'react-redux';

import CardOfPost from '../MainPage/CardOfPost/CardOfPost';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import Layout from '../Layout/Layout';
import './MainPage.scss';

class MainPage extends Component {
  componentWillMount(){
    this.props.getPost();
    this.props.getUser();
  }
  render() {
    const listPosts = this.props.postList.slice(0,10).map((inf) => <CardOfPost key={inf.id} postList={inf}/>);
    return (
      <div>
        <Header />
        <Container>
          <Row>
            <Col sm="12" md={{ size: 8, offset: 2 }} className="wrap-content">
              {listPosts}
            </Col>
          </Row>
        </Container>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
	return  {
    state,
    postList: state.post.postList,
    postUser: state.user.userList

	};
};

const mapDispatchToProps = (dispatch) => {
	return {
    getPost: () => dispatch(getPostFromAPI()),
    getUser: () => dispatch(getUserFromAPI())
  }
};

export default connect(mapStateToProps,mapDispatchToProps) (MainPage);
