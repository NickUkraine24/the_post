import React, { Component } from 'react';

import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import './About.scss';

class About extends Component {
  render() {
    return (
      <div>
        <Header />
        <Footer />
      </div>
    );
  }
}

export default About;
