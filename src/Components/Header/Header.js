import React, {Component} from 'react';
import {Link} from "react-router-dom";
import './Header.scss';

import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavLink } from 'reactstrap';

class Header extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
        isOpen: false
    };
  }
  toggle() {
    this.setState({
        isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <div>
        <Navbar color="dark" dark expand="md">
          <Link to="/home">
            <NavbarBrand href="/">The Post</NavbarBrand>
          </Link>
          <NavbarToggler onClick={this.toggle}/>
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <Link to="/about">
                <NavLink>
                  About
                </NavLink>
              </Link>
              <Link to="/contact"> 
                <NavLink>
                  Contact
                </NavLink>
              </Link>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

export default Header;
