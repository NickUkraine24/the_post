import React, {Component} from 'react';

import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import './Contact.scss';

class Contact extends Component {
  render() {
    return (
      <div>
        <Header />
        <Footer />
      </div>
    );
  }
}

export default Contact;
