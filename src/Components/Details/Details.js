import React, { Component } from 'react';

import { getPostFromAPI } from '../../actions/getPost';
import { getUserFromAPI } from '../../actions/getUser';
import { getCommentFromAPI } from '../../actions/getComment';
import { connect }  from 'react-redux';

import {
  Container, 
  Row, 
  Col, 
  Media } from 'reactstrap';
import CommentsOfPost from '../Details/CommentsOfPost/CommentsOfPost';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import Layout from '../Layout/Layout';

import './Details.scss';

class Details extends Component {
  constructor(props) {
    super(props);

    const { match: { params } } = this.props;

    this.getUserById = this.getUserById.bind(this);
    this.getPostById = this.getPostById.bind(this);
    this.getCommentById = this.getCommentById.bind(this);
    
    this.state = {
      id: params.id
    }
  }
  componentWillMount(){
    this.props.getUser();
    this.props.getPost();
    this.props.getComments();
  }
  getUserById(userId) {
    const user = this.props.postUser.filter(user => user.id === userId)[0];

    return user ? user : {};
  }
  getPostById(postId) {
    const { postList } = this.props;

    return postList.length ? postList.filter(data => data.id === +postId)[0] : {};
  }
  getCommentById(postId){
    const { postComments } = this.props;

    return postComments.length ? postComments.filter(comment => comment.id === +postId) : [];
  }
  render() {
    const post = this.getPostById(this.state.id),
      author = this.getUserById(post.userId),
      comment = this.getCommentById(this.state.id);
    const commentslist = comment.slice(0,50).map((comment) => <CommentsOfPost key={comment.id} postComments={comment}/>)
    return (
      <div>
        <Header />
        <Container>
          <Row className="row_details">
            <Col sm="12" md={{ size: 8, offset: 2 }}>
              <Media className="heading_media">
                {this.props.postList.length && this.getPostById(this.state.id).title}
              </Media>
            </Col>
            <Col sm="12" md={{ size: 3, offset: 9 }}>
              <Media className="author_media">
                {author.name}
              </Media>
            </Col>
            <Col sm="12" md={{ size: 10, offset: 1 }}>
              <Media className="body_media">
                {this.props.postList.length && this.getPostById(this.state.id).body}
              </Media>
            </Col>
            <Col sm="12" md={{ size: 8, offset: 2 }} className="col_comments">
              <Media heading className="title_comments_details">
                Comments
              </Media>
            </Col>
          </Row>
        </Container>
        {commentslist}
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
	return  {
    state,
    postList: state.post.postList,
    postUser: state.user.userList,
    postComments: state.comment.commentList

	};
};

const mapDispatchToProps = (dispatch) => {
	return {
    getPost: () => dispatch(getPostFromAPI()),
    getUser: () => dispatch(getUserFromAPI()),
    getComments: () => dispatch(getCommentFromAPI())
  }
};

export default connect(mapStateToProps,mapDispatchToProps) (Details);
